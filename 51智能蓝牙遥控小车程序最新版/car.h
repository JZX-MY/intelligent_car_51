#ifndef __car_H
#define __car_H

#include <reg52.h>
#include <intrins.h>

sbit Left_moto_pwm=P1^6 ;
sbit Right_moto_pwm=P1^7;
sbit p34=P3^4;
sbit p35=P3^5; 
sbit p36=P3^6;
sbit p37=P3^7;
sbit Trig= P1^4; //产生脉冲引脚
sbit Echo= P1^5; //回波引脚
sbit LED=P0^0;
sbit L_sensor=P2^0;
sbit R_sensor=P2^1;
void Left_moto_go() ;
void Left_moto_back() ;
void Left_moto_stp() ;
void Right_moto_go();
void Right_moto_back(); 
void Right_moto_stp(); 
void delay(unsigned int k) ;
void delay1s(void) ;
void delay1ms(void);
void pwm_out_left_moto(void) ;
void pwm_out_right_moto(void);
void run(void);
void back(void);
void left(void);
void right(void);
void stop(void);
void rotate(void);
void  StartModule() ;
void Timer1Init();
void Timer0Init();
void Conut(void);
void Timer1Init2();
void  Speed_add() ;
void  Speed_reduce();


#endif