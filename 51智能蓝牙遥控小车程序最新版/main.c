#include <car.h>

extern unsigned char Left_Speed_Ratio;
extern unsigned char Right_Speed_Ratio;
unsigned int time=0; 
unsigned int HC_SR04_time=0;
extern unsigned char pwm_val_left;
extern unsigned char pwm_val_right;
 bit   flag =0;
extern char M_sensor;  
char Work_Mode=0;   //工作模式的选择  为0时，为手机或者电脑等上位机对小车进行蓝牙遥控 ，为1时小车自动避障模式
char Work_Mode2=0;
unsigned char receive_data=0;
unsigned char receive_real_data=0;
void delay1s(void)   
{
    unsigned char a,b,c;
    for(c=167;c>0;c--)
        for(b=171;b>0;b--)
            for(a=16;a>0;a--);
    _nop_();  
}
void delay1ms(void)   
{
    unsigned char a,b,c;
    for(c=1;c>0;c--)
        for(b=142;b>0;b--)
            for(a=2;a>0;a--);
}

void Timer0Init()
{
	TMOD|=0X01;//选择为定时器0模式，工作方式1，仅用TR0打开启动。

	TH0=0XFC;	//给定时器赋初值，定时1ms
	TL0=0X18;	
	ET0=1;//打开定时器0中断允许
	EA=1;//打开总中断
	TR0=1;//打开定时器			
}

void Timer1Init()
{
	TMOD=0X11;//选择为定时器1模式，工作方式1，仅用TR1打开启动。选择为定时器0模式，工作方式1，仅用TR1打开启动
	TH1=0;	
	TL1=0;	
	ET1=1;//打开定时器1中断允许
	EA=1;//打开总中断
	TR1=1;//打开定时器			
}



void Timer1Init2()
{
		SCON=0X50;			//设置为工作方式1,8位数据，可变波特率
	  TMOD |=0X20;			//设置计数器工作方式2
	  PCON=0X00;			//波特率不加倍
	  TH1=0XFd;		    //计数器初始值设置，9600  @11.0592MHz
	  TL1=0XFd;
	  TR1=1;					//打开计数器
	  ES = 1;         //开串口中断
    EA = 1;         //开总中断
}




void timer0()interrupt 1 using 2 
{ 
TH0=0XFC;	//给定时器赋初值，定时1ms
TL0=0X18;
time++; 
pwm_val_left++; 
pwm_val_right++; 
pwm_out_left_moto(); 
pwm_out_right_moto(); 
	
HC_SR04_time++;
if(HC_SR04_time>=500)   //500ms 启动一次超声波测距
{
	HC_SR04_time=0;
	StartModule();
}
} 

void Timer1() interrupt 3
{
	flag=1;    //若定时器1溢出则flag置1
}


void Com_Int(void) interrupt 4
{
    EA = 0;	
  
  if(RI == 1) //当硬件接收到一个数据时，RI会置位
	{ 
   	LED=0;
		RI = 0;
		receive_data = SBUF;//接收到的数据
		if(receive_data!=0)
		receive_real_data=receive_data;
		
	  switch(receive_real_data) 
          { 
		      case '5': Speed_add(); break; 
      	  case '6': Speed_reduce(); break; 
	        }
		
		SBUF=receive_real_data;//将接收到的数据放入到发送寄存器
	  while(!TI);			 //等待发送数据完成
	  TI=0;						 //清除发送完成标志位
		
		
}
    EA = 1;	
}

void main()
	{
	Timer0Init();
	Timer1Init2();	
	Left_Speed_Ratio=5;   //设置左电机车速为最大车速的50%
	Right_Speed_Ratio=5;	////设置右电机车速为最大车速的50%
	while(1)
		{
		  if(Work_Mode==1)
			{
			   	ES=0;   //关闭串口中断
				  
				 if(Work_Mode2==0)
				 {
					
					Timer1Init();
           Work_Mode2=1;
					  
				 }
					 
				
						if(Echo==1)
				{
				TH1=0;	
	      TL1=0;
				TR1=1;			    //开启计数	
				while(Echo);			//当RX为1计数并等待	
				TR1=0;				//关闭计数	
			  Conut();			//计算		
			}
				
		  if(M_sensor==1)
				{  run(); }
		  else
			  {
			      if(L_sensor==1)
				       {     left();       }
			
			      else if(R_sensor==1)
				       {    right() ;      }
			      else
						   {    back();          }
			
			 }
					
			}
			else
				
			{
				
			switch(receive_real_data) 
        { 
        
		      case '1': run(); break; 
		      case '2': left(); break; 
		      case '3': right(); break; 
	     	  case '4': back(); break; 
				  case '7': stop(); break; 
	        case '8': Work_Mode=1; break; 
	        }
			
				
		    LED=1;
     }	
		}
}

